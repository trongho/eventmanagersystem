﻿using EventManager.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventManager.SignalR
{
    public class LiveUpdateSignalRHub:Hub
    {
        private readonly StockTicker _stockTicker;

        public LiveUpdateSignalRHub(StockTicker stockTicker)
        {
            _stockTicker = stockTicker;
        }

        public IEnumerable<Stock> GetAllStocks()
        {
            return _stockTicker.GetAllStocks();
        }
    }
}
