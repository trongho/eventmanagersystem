﻿using EventManager.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventManager.SignalR
{
    public class LiveUpdateElectionHub:Hub
    {
        private readonly ElectionTicker _electionTicker;

        public LiveUpdateElectionHub(ElectionTicker electionTicker)
        {
            _electionTicker = electionTicker;
        }

        public IEnumerable<ElectionModel> GetAllElections()
        {
            return _electionTicker.GetAllElections();
        }

        public  Task SendOpenVotePopupAsync()
        {
            return  Clients.All.SendAsync("sendOpenVotePopup",true);
        }

        public Task SendCloseVotePopupAsync()
        {
            return Clients.All.SendAsync("sendCloseVotePopup",false);
        }
    }
}
