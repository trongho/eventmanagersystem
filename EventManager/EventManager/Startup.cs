using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using EventManager.Entitys;
using EventManager.Models;
using EventManager.SignalR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EventManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services
                .AddControllersWithViews()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

            services.AddDbContext<NotifEntities>(options => options.UseSqlServer(Configuration.GetConnectionString("DbConnectionString")));

            services.AddCors(options => options.AddPolicy("CorsPolicy", builder => {
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowed(origin => true) // allow any origin 
                    .AllowCredentials();
            }));
            services.AddSignalR().AddJsonProtocol(options => { options.PayloadSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;});
            services.AddScoped<LiveUpdateSignalRHub>();
            services.AddSingleton<StockTicker>();
            services.AddScoped<LiveUpdateElectionHub>();
            services.AddSingleton<ElectionTicker>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("CorsPolicy");

            app.UseWebSockets();


            app.UseSignalR(routes =>
            {
                routes.MapHub<LiveUpdateSignalRHub>("/liveUpdateSignalRHub");
                //routes.MapHub<LiveUpdateElectionHub>("/liveUpdateElectionHub");
            });



            app.UseEndpoints(endpoints => {
                //endpoints.MapHub<LiveUpdateSignalRHub>("/liveUpdateSignalRHub"); // Restore this
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapHub<LiveUpdateElectionHub>("/liveUpdateElectionHub");

            });
        }
    }
}
