﻿using EventManager.SignalR;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EventManager.Models
{
    public class ElectionTicker
    {
        private readonly IEnumerable<ElectionModel> _elections;
        private IHubContext<LiveUpdateElectionHub> _hubContext { get; set; }

        private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(500);
        private readonly Random _updateOrNotRandom = new Random();

        private readonly Timer _timer;

        private readonly object _updateElectionVotesLock = new object();

        static readonly Random random = new Random();

        public ElectionTicker(IHubContext<LiveUpdateElectionHub> hubContext)
        {
            _hubContext = hubContext;

            _elections = GenerateElections();

            _timer = new Timer(UpdateElectionVotes, null, _updateInterval, _updateInterval);

        }

        public IEnumerable<ElectionModel> GetAllElections()
        {
            return _elections;
        }

        static IEnumerable<ElectionModel> GenerateElections()
        {
            return new[] {
                new ElectionModel() { Id =1, FullName="Hồ Kim Trọng", Birthday = DateTime.Parse("1990-10-28"),Votes=0},
                new ElectionModel() { Id =2, FullName="La Đặng Gia Bảo", Birthday = DateTime.Parse("1995-07-21"),Votes=0},
                new ElectionModel() { Id =3, FullName="Đỗ Trọng Mẫn", Birthday = DateTime.Parse("1995-11-02"),Votes=0},
                new ElectionModel() { Id =4, FullName="Hoàng Anh Vũ", Birthday = DateTime.Parse("1970-10-01"),Votes=0},
                new ElectionModel() { Id =5, FullName="Trần Đình Lợi", Birthday = DateTime.Parse("1999-07-20"),Votes=0},
                new ElectionModel() { Id =6, FullName="Lê Văn Cầu", Birthday = DateTime.Parse("1987-01-13"),Votes=0},
            };
        }
        private void UpdateElectionVotes(object state)
        {
            lock (_updateElectionVotesLock)
            {
                foreach (var election in _elections)
                {
                    if (TryUpdateElectionVotes(election))
                    {
                        BroadcastElectionVotes(election);
                    }
                }
            }
        }

        private bool TryUpdateElectionVotes(ElectionModel election)
        {
            var r = _updateOrNotRandom.NextDouble();
            if (r > .1)
            {
                return false;
            }

            election.Update();
            return true;
        }

        private void BroadcastElectionVotes(ElectionModel election)
        {
            _hubContext.Clients.All.SendAsync("updateElectionVotes",election);
        }

    }
}
