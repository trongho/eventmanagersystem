﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventManager.Models
{
    public class NotifModels
    {
        public string UserID { get; set; }
        public string Message { get; set; }
    }
}
