﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventManager.Models
{
    public class ElectionModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime Birthday { get; set; }
        public int Votes { get; set; }

        int change = 0;

        public int Change
        {
            get
            {
                return change;
            }
        }

        public void Update()
        {
            change = GenerateChange();
            int newVotes =Votes+change;
            Votes =newVotes;
        }

        static Random random = new Random();

        int GenerateChange()
        {
            return (int)random.Next(0,10);
        }
    }
}
