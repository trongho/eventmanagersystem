﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventManager.Entitys
{
    public class NotifEntities : DbContext
    {
        public DbSet<Notification> Notifications { get; set; }
        public NotifEntities(DbContextOptions<NotifEntities> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Notification>().ToTable("Notification").HasKey(e =>e.Id);
        }

    }
}
