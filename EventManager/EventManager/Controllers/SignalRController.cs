﻿using EventManager.Entitys;
using EventManager.Models;
using EventManager.SignalR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace EventManager.Controllers
{
    public class SignalRController : Controller
    {
        private readonly NotifEntities context;

        public SignalRController(NotifEntities context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public HttpResponseMessage SendNotification([FromBody]NotifModels obj)
        {
            NotificationHub objNotifHub = new NotificationHub(context);
            Notification objNotif = new Notification();
            objNotif.Id = 1;
            objNotif.SentTo = obj.UserID;

            //context.Configuration.ProxyCreationEnabled = false;
            context.Notifications.Add(objNotif);
            context.SaveChanges();

            objNotifHub.SendNotification(objNotif.SentTo);

            var query = (from t in context.Notifications
                         select t).ToList();


            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
